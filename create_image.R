# making the plot
#library(raster)
#library(tidyverse, lib.loc = "/export/home/denhamr/R/x86_64-pc-linux-gnu-library/3.6")
library(ggplot2, lib.loc = "/export/home/denhamr/R/x86_64-pc-linux-gnu-library/3.6")
library(sf)
print(load("saved_objects/frac_details.RData"))


rasterbg <- raster::raster("data/gray_50m_sr_ob_aust.tif")
# crop to right size. Has to be 1687 x 1050
# 

e <- raster::extent(rasterbg, 150+0,1233+30, 30+33,2100-43)
rasterbg2 = raster::crop(rasterbg, e)
bg_spdf <- as(rasterbg2, "SpatialPixelsDataFrame")
bg_df <- as.data.frame(bg_spdf)
colnames(bg_df) <- c("value", "x", "y")

# first we do a sense check
# load a single queensland route and see if it checks out.

# and get the values
qfname <- list.files(path="output_objects/", pattern="path_.*.RData", full.names = TRUE)
qpath = list()
total_length = 0.0
unduped_length = 0.0

units(total_length) = "m"
units(unduped_length) = "m"
trip_lengths <- rep(NA, length(qfname))

for(i in 1:length(qfname)) {
  objname <- load(qfname[i])
  
  # we want to make sure that we don't have duplicate in there.
  # otherwise 
  p1lengths <- thispath %>% 
    st_transform(3577) %>% 
    st_length()/1000
  
  bb <- thispath %>% mutate(lengthdups = duplicated(round(p1lengths,2)))
  
  trip_len = bb %>% 
    filter(!lengthdups) %>% 
    st_transform(3577) %>% 
    st_length() %>% sum()
  
  trip_lengths[i] <- trip_len
  total_length <- total_length + trip_len
  
  unduped_length <- unduped_length + thispath %>% 
    st_transform(3577) %>% 
    st_length() %>% sum()

    qpath[i] <- list(get(objname))
}

# convert to km
units(total_length) = "km"
print(total_length)

units(unduped_length) = "km"
print(unduped_length)


qpath <- do.call('rbind', qpath)

# I experimented with scale and width
# a width of 11.24667 is about right, but
# it gives 1619
# so need to scale it a bit
thewidth = 11.24667*(1678/1619)
theheight = thewidth *(1050/1678)


ggplot() +  
  geom_raster(data=bg_df, aes(x=x, y=y, fill=value), alpha=0.8) +
  geom_sf(data=qpath, alpha = 0.2, size = 0.4, color = "#f9ba00f0") +
  geom_sf(data=frac_details, colour="white", alpha=0.2, size=1.0, shape=16)  +
  scale_x_continuous(expand=c(0,0)) + 
  scale_y_continuous(expand=c(0,0)) +
  scale_fill_gradient("value",low="black", high=grey(0.5)) +
  theme(panel.background = element_rect(fill = "black", colour = "#05050f"), 
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), 
        axis.title = element_blank(), 
        axis.text = element_blank(), 
        axis.ticks.length = unit(0, "cm"),
        legend.position = "none",
        plot.margin = margin(0, 0, 0, 0, "cm"),) +
  coord_sf()
ggsave('iotm_large.png', width=thewidth, height=theheight, dpi=72, scale=2)


# to do the next one we need a different ratio.
# I'll crop the image again
# this needs to be skinny, so make the width longer, and we'll just crop
rasterbg <- raster::raster("data/gray_50m_sr_ob_aust_wide.tif")
bg_spdf <- as(rasterbg, "SpatialPixelsDataFrame")
bg_df <- as.data.frame(bg_spdf)
bg_df$value[is.na(bg_df$value)] <- NA
colnames(bg_df) <- c("value", "x", "y")

ggplot() +  
  geom_raster(data=bg_df, aes(x=x, y=y, fill=value), alpha=0.8) +
  geom_sf(data=qpath, alpha = 0.2, size = 0.4, color = "#f9ba00f0") +
  geom_sf(data=frac_details, colour="white", alpha=0.2, size=0.5, shape=16)  +
  scale_x_continuous(expand=c(0,0)) + 
  scale_y_continuous(expand=c(0,0)) +
  scale_fill_gradient("value",low="black", high=grey(0.5)) +
  theme(panel.background = element_rect(fill = "black", colour = "#05050f"), 
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(), 
        axis.title = element_blank(), 
        axis.text = element_blank(), 
        axis.ticks.length = unit(0, "cm"),
        legend.position = "none",
        plot.margin = margin(0, 0, 0, 0, "cm"),) +
  coord_sf()
ggsave('iotm_medium.png', width=thewidth*0.25, height=theheight*0.25, dpi=72, scale=1.5)


