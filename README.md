Image of the Month February 2020
================================



Behind the Product
==================


  * There is a lot more to a remotely sensed product than meets the eye.
  * To produce a meaningful product, we first have to relate what we see on the ground to the data
  available from the satellite.
  * This often involves a lot of field work, data analysis, science and computer processing.
  * This is a collaborative effort, involving people throughout Australia, and helps to produce a
  product used throughout the world.
  * This image illustrates the amount of field work that was used to derive the upcoming
    fractional cover product.
  * Each point indicates a field site, and the lines indicates the route travelled to each site.
  Brighter lines indicate more commonly traversed roads.
  * Over 260 individual trips were made to collect over 4,300 individual sites. We estimate this
   means more than 3 million kilometres were driven to collect this data.
  * It is important to capture the range of conditions across Australia. This image shows that we
  have information even from remote areas of Australia; from The Kimberley, through the Simpson
  Desert, down to Tasmania.
  * The field sites were used to train a model to predict the fraction of the earth that consists
  of bare ground, dry vegetation and green vegetation. At each site, a team collected information on the
  amount and type of vegetation. This was then used to derive a model allowing us to make predictions
  from satellite images.
  * All states and territories are represented. Staff from Federal Government Departments,
  State Government, Universities and the general public have all contributed to the data.
  * Some trips were dedicated to this specific project, but most trips were more general, collecting a
  range of useful data.
  * Fractional cover has been used to help understand grazing condition, model soil erosion and
  assess the impact of human activities.
  * A new version of this product will be released later this year.


See http://data.auscover.org.au/xwiki/bin/view/Product+pages/Landsat+Seasonal+Fractional+Cover


Running the Code
----------------

```bash
bbox="137.995957,-29.177898,153.552171,-10.057"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o qld-cropped.osm.pbf

for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter qld-cropped.osm.pbf w/highway=${i} -o qld-${i}-highways.osm.bz2
done

# unzip them
for i in *bz2
do
  bunzip2 $i
done

bbox="129,-25.99948,138.00120,-10.96591 "
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o nt-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway' 'tertiary'
do 
    osmium tags-filter nt-cropped.osm.pbf w/highway=${i} -o nt-${i}-highways.osm.bz2
done

# unzip them
for i in nt*bz2
do
  bunzip2 $i
done
# for NSW and ACT
# 140.99947 -37.50506 159.10922 -28.15702 
state=nsw
bbox="140.99947,-37.50506,159.10922,-28.15702"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o ${state}-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter ${state}-cropped.osm.pbf w/highway=${i} -o ${state}-${i}-highways.osm.bz2
done

# unzip them
for i in ${state}*bz2
do
  bunzip2 $i
done

# for vic
# 140.96168 -39.15919 149.97668 -33.98043 
state=vic
bbox="140.96168,-39.15919,149.97668,-33.98043"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o ${state}-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter ${state}-cropped.osm.pbf w/highway=${i} -o ${state}-${i}-highways.osm.bz2
done

# unzip them
for i in ${state}*bz2
do
  bunzip2 $i
done

# for tassie
state=tas
bbox="143.81892,-43.74051,148.49867,-39.20366"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o ${state}-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter ${state}-cropped.osm.pbf w/highway=${i} -o ${state}-${i}-highways.osm.bz2
done

# unzip them
for i in ${state}*bz2
do
  bunzip2 $i
done

state="sa"
bbox="129.00134,-38.06260,141.00296,-25.99615"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o ${state}-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter ${state}-cropped.osm.pbf w/highway=${i} -o ${state}-${i}-highways.osm.bz2
done
for i in ${state}*bz2
do
  bunzip2 $i
done

state="wa"
bbox="112.92111,-35.13485,129.00193,-13.68949"
osmium extract --strategy complete_ways --bbox ${bbox} australia-latest.osm.pbf -o ${state}-cropped.osm.pbf
for i in 'primary' 'secondary' 'trunk' 'motorway'
do 
    osmium tags-filter ${state}-cropped.osm.pbf w/highway=${i} -o ${state}-${i}-highways.osm.bz2
done
for i in ${state}*bz2
do
  bunzip2 $i
done

```


Basic parallel running was done using the PBS batch queue, a template is below. Different templates are used for different states, but they are pretty much all the same.

```bash

#!/usr/bin/env bash
#PBS -l select=1:ncpus=1:mem=2gb
#PBS -l walltime=2:00:00
#PBS -A rscstd
#PBS -N nttripidXXX
#PBS -V
source /etc/profile.d/modules.sh
umask 0002

singularity exec /scratch/containers/denhamr/rstudio_geo.sif \
	    Rscript \
	    /export/home/denhamr/tasks/feb20/iotm/route_calculation_nt.R XX
```

Submission like:

```bash
subname=../saved_objects/nt_sub_244.RData
for subname in ../saved_objects/nt_sub_*.RData
do
 tripid=$(basename $subname|perl -pne 's/nt_sub_(\d+).RData/$1/')
 jobfname="job_nt_${tripid}.sh"
 perl -pne "s/XXX/$tripid/g" job_nt_template.sh > $jobfname
 qsub $jobfname
done
```

